<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
	
<?php

include ("conexion.php")
  
  
?>

<html>
	
	<h1>¿Necesitas ayuda para decidir? ¿Demasiadas dudas en tu cabeza?</h1>
	<p>
	Deja que aquí te ayudemos a tomar una decisión. La comunidad leerá tu historia y te ayudará entre las opciones que tengas.<br>
	
	<a href="nueva_peticion.php">Cuenta tu historia a la comunidad y recibe consejos.</a></p>
	<p></p>
		<h2>¿Quieres dar consejos a alguien? </h2>
		<p>
		Muchas personas están esperando a que alguien les ayude a tomar una decisión.<br>
		Elige una categoría:<br>
		<input class="MiButton" type="button" value="GENERAL" onclick="window.location.href='ver_peticiones.php?ver=GENERAL'"/>
		<input class="MiButton" type="button" value="MASCOTAS" onclick="window.location.href='ver_peticiones.php?ver=MASCOTAS'"/>
		<input class="MiButton" type="button" value="TRABAJO" onclick="window.location.href='ver_peticiones.php?ver=TRABAJO'"/>
		<input class="MiButton" type="button" value="AMOR Y PAREJA" onclick="window.location.href='ver_peticiones.php?ver=AMOR-PAREJA'"/>
		<input class="MiButton" type="button" value="SALUD Y DEPORTE" onclick="window.location.href='ver_peticiones.php?ver=SALUD-DEPORTE'"/>
		<input class="MiButton" type="button" value="FAMILIA Y AMIGOS" onclick="window.location.href='ver_peticiones.php?ver=FAMILIA-AMIGOS'"/>
		
		
		
	</p>
<p>
	También puedes buscar por el nombre de la persona. Para encontrar una duda concreta o ver las respuestas a tu consulta.
	Utiliza este buscador: <br>
	<form method="post" action="buscar.php">
		Nombre o apodo de la persona a buscar: <input type="text" name="nombre">
		<input type="submit" value="Buscar"><br>
		
	</form>
	
</p>
	
<footer>
	<input class="MiButton" type="button" value="Inicio" onclick="window.location.href='index.php'"/>	
</footer>
		
</html>